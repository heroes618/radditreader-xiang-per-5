package us.theappacademy.redditreader;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


//this adapter is going to adapted the holder
public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostHolder> {

    private ArrayList<RedditPost> redditPosts;    // this string redditposts is the same as the public string redditposts
    private  ActivityCallback activityCallback;

    public RedditPostAdapter(ActivityCallback activityCallback){
        redditPosts = RedditPostParser.getInstance().redditPosts;  //this . redditposts we r talking about private string
        this.activityCallback = activityCallback;
    }

    @Override
    public RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view); // everytime we created a view holder  we going to inflated this layout simple list item which is text view and we r going to created a new redditpostholder view
    }

    @Override
    public void onBindViewHolder(RedditPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri redditUri = Uri.parse(redditPosts.get(position).url);
               activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(redditPosts.get(position).title); //titleText is our text view we need the textview inorder to display the text

    }

    @Override
    public int getItemCount() {
        return redditPosts.size(); //there is no . for array

    }
}
