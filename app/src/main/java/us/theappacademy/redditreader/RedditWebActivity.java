package us.theappacademy.redditreader;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.List;

public class RedditWebActivity extends AppCompatActivity { //we want to be compaclible to the older verison of the android

    private ViewPager viewPager;
    private List<RedditPost> redditPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reddit_web); //anytime we created somethink like this we have to tell manifest
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        redditPosts = RedditPostParser.getInstance().redditPosts;

        Intent intent = getIntent();
        Uri redditUri = intent.getData();

        FragmentManager fragmentManager = getSupportFragmentManager(); // this fragmentmanager is for viewpager so the viewpager know how to manage the viewpage
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                RedditPost redditPost = redditPosts.get(position);
                return RedditWebFragment.newFragemnt(redditPost.url);
            }

            @Override
            public int getCount() {
                return redditPosts.size();
            }
        });

        for(int index = 0; index < redditPosts.size(); index++) { //we want to chek each reddit
            if(redditPosts.get(index).url.equals(redditUri.toString())){ //check is the reddit is true
                viewPager.setCurrentItem(index);
                break; //once we use the viewpage than we dont want use that function
            }
        }


    }
}
