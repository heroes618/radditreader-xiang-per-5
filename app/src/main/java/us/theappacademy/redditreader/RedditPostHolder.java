package us.theappacademy.redditreader;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Student on 9/21/2015.
 */
public class RedditPostHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public RedditPostHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
