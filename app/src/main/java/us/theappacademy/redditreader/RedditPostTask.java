package us.theappacademy.redditreader;


import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RedditPostTask extends AsyncTask<RedditListFragment, Void, JSONObject> {
  private RedditListFragment redditListFragment;


    @Override
    protected JSONObject doInBackground(RedditListFragment... redditListFragments) { //we want our app to responesive and pick up internet
                JSONObject jsonObject = null;
        redditListFragment = redditListFragments[0];
        try {
            URL redditFeedUrl = new URL("https://reddit.com/r/android.json");

            HttpURLConnection httpConnection = (HttpURLConnection)redditFeedUrl.openConnection();
            httpConnection.connect();

            int statusCode = httpConnection.getResponseCode();

            if(statusCode == HttpURLConnection.HTTP_OK) { //this code checks our states code to 200
                jsonObject = RedditPostParser.getInstance().parseInputStream(httpConnection.getInputStream()); //any time we use redditpostparser we have to use getinstance
            }
        }

        catch (MalformedURLException error) { // we r going to execute the url if we have a error then the malformedurlexception can show us error
            Log.e("RedditPostTask", "MalformedURLException  (doInBackground) :" + error);
        }
        catch (IOException error){
            Log.e("RedditPostTask", "IOException (doInBackground) :" + error);
        }
        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) { //this postexecute is want execute our project
        RedditPostParser.getInstance(). readRedditFeed(jsonObject);
        redditListFragment.updateUserInterface();
    }
}
